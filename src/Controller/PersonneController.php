<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PersonnesRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PersonneController extends AbstractController
{

    private $personnesRepository;

    public function __construct(PersonnesRepository $personnesRepository)
    {
        $this->personnesRepository = $personnesRepository;
    }

    /**
     * @Route("/personne", name="personne")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/PersonneController.php',
        ]);
    }


    /**
     * @Route("/personnes", name="get_all_personnes", methods={"GET"})
    */
    public function getAll(): JsonResponse
    {
        $personnes = $this->personnesRepository->findAll([], ['username' => 'ASC']);
        $data = [];

        foreach ($personnes as $personne) {
            $data[] = [
                'id' => $personne->getId(),
                'firstname' => $personne->getFirstName(),
                'lastname' => $personne->getLastName(),
                'birth_date' => $personne->getBirthDate()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/personne/add", name="add_user", methods={"POST"})
    */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $birth_date = $data['birth_date'];
        
        $age = date_diff(date_create($birth_date), date_create('now'))->y;
        //dd($age);

        if (empty($firstname) || empty($lastname) || empty($birth_date) ) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }
        if($age < 150){
            $this->personnesRepository->savePersonne($firstname, $lastname, $birth_date);
        }else{
            throw new NotFoundHttpException('Sorry, You are so old!');
        }

        return new JsonResponse(['status' => 'User created!'], Response::HTTP_CREATED);
    }
}
