<?php

namespace App\Entity;

use App\Repository\PersonnesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonnesRepository::class)
 */
class Personnes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="string", length=255)
    */
    private $firstname;

    public function getFirstName(): ?string
    {
        return $this->firstname;
    }

    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @ORM\Column(type="string", length=255)
    */
    private $lastname;

    public function getLastName(): ?string
    {
        return $this->lastname;
    }

    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @ORM\Column(type="string", length=255)
    */
    private $birth_date;

    public function getBirthDate(): ?string
    {
        return $this->birth_date;
    }

    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
    }


    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'firstname' => $this->getFirstName(),
            'lastname' => $this->getLastName(),
            'birth_date' => $this->getBirthDate()
        ];
    }

}
