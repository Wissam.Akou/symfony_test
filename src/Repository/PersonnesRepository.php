<?php

namespace App\Repository;

use App\Entity\Personnes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Personnes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Personnes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Personnes[]    findAll()
 * @method Personnes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonnesRepository extends ServiceEntityRepository
{
    public function __construct(
            ManagerRegistry $registry,
            EntityManagerInterface $manager
        )
    {
        parent::__construct($registry, Personnes::class);
        $this->manager = $manager;
    }

    // /**
    //  * @return Personne[] Returns an array of Pseronnes objects
    //  */
    
    public function savePersonne($firstname, $lastname, $birth_date)
    {
        $newPersonne = new Personnes();

        $newPersonne->setFirstName($firstname);
        $newPersonne->setLastName($lastname);
        $newPersonne->setBirthDate($birth_date);
        $this->manager->persist($newPersonne);
        $this->manager->flush();
    }

  
    
}
