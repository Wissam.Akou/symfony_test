After cloning the project flow what written in the section below

```bash
    $ composer install
    $ composer dump-autoload
    $ symfony server:start
```
## Creation API project

```bash
    $ composer create-project symfony/website-skeleton project-name
```
## Install of dependencies 

```bash
    $ composer require symfony/serializer
    $ composer require doctrine/annotationsdump-autoload
    $ composer require friendsofsymfony/rest-bundle
    $ composer require symfony/orm-pack
    $ composer require symfony/property-access
    $ composer require symfony/web-server-bundle --dev
    $ composer require symfony/maker-bundle --dev
    $ symfony composer req maker --dev
    $ composer require --dev orm-fixtures
    $ composer require nelmio/api-doc-bundle:^3.0
```


## Configure .env

```bash
    $ DB_USER=root
    $ DB_PASSWORD=
    $ DB_HOST=localhost
    $ DB_PORT=3306
    $ DB_NAME=symfony-api
    $ DATABASE_URL="mysql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
```

## Create Database && entities

After Configuring the .env file run the below commands

```bash
    $ php bin/console doctrine:database:create 
    $ php bin/console make:entity // Create Entity and it Repository 
```

## Make Migrations && Seeders

```bash
    ### Migration
    $ php bin/console make:migration
    $ php bin/console doctrine:migrations:migrate 
    ### Fixtures
    $ composer require --dev orm-fixtures
    $ bin/console doctrine:fixtures:load 
```

## Create Controller

```bash
    $ symfony console make:controller personneController
```

## API DOC
```bash
    http://127.0.0.1:8000/api/doc.json
```

# Chemin XPath

1 - /child:book
2 - /child::book/child::title/attribute::type (roman)
3 - count (/child::book/child::title/attribute::type) (bd)